drop DATABASE if exists pruebaPhp;

create DATABASE pruebaPhp;

use pruebaPhp;

create table autores(
    id int auto_increment,
    nombre varchar(100),
    apellidos varchar(100),
    primary key(id)
);

insert into autores (nombre, apellidos) values
('Javier', 'Cordero García'),
('Miguel', 'Herrera');
