<?php
// precarga de clases

use clases\Autores;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

// var_dump(Autores::listar());

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>

    <div class="row mt-3">
        <?php
        require "_menu.php";
        ?>
    </div>
    <div class="container mt-3">
        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                <h1>Listar con Modelo</h1>
                <div class="lead">
                    Listado de registros de la tabla autores
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <?= Autores::gridView([
                "eliminar" => "eliminar.php",
                "actualizar" => "actualizar.php"
            ]) ?>
        </div>
    </div>
</body>

</html>