<?php

use clases\Autores;
use clases\Consultas;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$mensaje = "";
// llegan los datos del formulario
if ($_POST) {

    // creo un autor vacio
    $autor = new Autores();
    // utilizo el metodo obtener para realizar la asignacion masiva
    $autor->obtener($_POST)->insert();

    // puedo insertar los datos a mano
    // $autor->nombre = $_POST["nombre"];
    // $autor->apellidos = $_POST["apellidos"];
    // $autor->insert();

    // preparo el mensaje
    $mensaje = "Registro insertado";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="row mt-3">
        <?php
        require "_menu.php";
        ?>
    </div>
    <div class="container">
        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                <h1>Insertar Registro con Clase Consultas</h1>
                <div class="lead">
                    Introducir registro en la tabla autores
                </div>
            </div>
        </div>
        <?php
        // mostrar mensaje de registro insertado
        if (!empty($mensaje)) {
        ?>
            <div class="row mt-3">
                <div class="alert alert-success">
                    <?= $mensaje ?>
                </div>
            </div>
        <?php
        }
        ?>
        <div class="row mt-3">
            <form method="post" class="col-lg-6">
                <div class="input-group mb-3">
                    <span class="input-group-text">Nombre</span>
                    <input type="text" name="nombre" class="form-control">
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text">Apellidos</span>
                    <input type="text" name="apellidos" class="form-control">
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">
                        Insertar
                    </button>
                    <button type="reset" class="btn btn-danger">
                        Borrar
                    </button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>