<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Ejercicio 6</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Inicio Modelo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="indexConsultas.php">Inicio Consultas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="insertar.php">Insertar Modelo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="insertarConsultas.php">Insertar Consultas</a>
                </li>
            </ul>
        </div>
    </div>
</nav>