<?php

use Clases\Ejercicio2\Imagen;

require_once 'autoload.php';


$imagen1 = new Imagen('foto3.png', 100, 100);

var_dump($imagen1);

$imagen2 = new Imagen('foto1.jpg', 100, 100);

var_dump($imagen2);

$imagen3 = new Imagen('foto2.jpg', 100, 100);

var_dump($imagen3);

$imagen4 = new Imagen('foto4.jpg', 100, 100);

var_dump($imagen4);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <?= $imagen1 ?>
    <?= $imagen2 ?>
    <?= $imagen3 ?>
    <?= $imagen4 ?>
</body>

</html>