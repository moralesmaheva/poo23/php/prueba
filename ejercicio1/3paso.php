<?php

//tengo que comprobar si todas las cajas contienen algo
//tengo que indicar cuantas estan vacias y cuantas hay en total

function vacias(array $vector): int
{
    $contadorVacias = 0;
    foreach ($vector as $valores) {
        if (empty($valores)) {
            $contadorVacias++;
        }
    }
    return $contadorVacias;
}

//opcion 2 de la funcion vacias
function vacias2(array $vector): int
{
    //para comprobar que hay celdas vacias 
    $contadorVacias = 0;
    $arrayRepeticiones = array_count_values($vector);
    if (array_key_exists("", $arrayRepeticiones)) {
        $contadorVacias = $arrayRepeticiones[""];
    }
    return $contadorVacias;
}

//tengo que comprobar si hay repetidos en los datos recibidos

function repetidos(array $vector): bool
{
    //array_count_values te devuelve un array con las repeticiones y el numero de veces que hay
    //como  valor
    $arrayRepeticiones = array_count_values($vector);
    //supongo que si hay repeticiones devuelve true
    $salida = true;
    //si el array resultado es igual al tamaño del vector no hay repeticiones
    if (count($arrayRepeticiones) == count($vector)) {
        $salida = false;
    }
    return $salida;
}

//opcion 2 de la funcion repetidos
function repetidos2(array $vector): bool
{
    $salida = true;
    //es el mismo array pero sin repetidos
    $sinRepetidos = array_unique($vector);
    if (count($sinRepetidos) == count($vector)) {
        $salida = false;
    }
    return $salida;
}


//si no has enviado el formulario redirecciona al 1
if (!$_POST) {
    header("Location: 1paso.php");
}

$valores = $_POST["caja"];

$numeroVacias = vacias($valores);
$total = count($valores);
$rellenas = $total - $numeroVacias;
$hayRepetidos = repetidos($valores);

//crear los mensajes de salida

if (!$numeroVacias) {
    $menaje = "Has rellenado las {$total} cajas";
} else {
    $mensaje = "Hay {$rellenas} cajas rellenas de un total de {$total}";
}

$mensaje1 = $hayRepetidos ? "Hay repeticiones" : "No hay repeticiones";


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paso 3</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="color1 rounded text-color1 p-3">
                <h1>Ejercicio Numero 1 del examen de PHP</h1>
                <div class="lead">Paso 3 - Estadisticas</div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="card col-lg-4 color1 text-color1">
                <div class="card-header">
                    <h3>Resultados</h3>
                </div>
                <div class="card-body">
                    <p class="card-text"><?= $mensaje ?></p>
                    <p class="card-text"><?= $mensaje1 ?></p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card color1 text-color1 offset-lg-3 p-2">
            <div class="card-body">
                <h3 class="card-title">Valores escritos</h3>
            </div>
            <div class="card-text">
                <ul class="list-group">
                    <?php
                    foreach ($valores as $valor) {
                        echo "<li class='list-group-item'>";
                        echo $valor ?: "Vacía";
                        echo "</li>";
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="row mt-5 mb-0">
            <div class="text-color1 p-5 alert color1">
                Ejercicio del examen de PHP - Maheva Morales
            </div>
        </div>

        <div class="row mt-2">
            <img src="../images/foto4.jpg" class="col-lg-2 col-sm-2 d-block mx-auto">
        </div>
    </div>
</body>

</html>