<?php
//en caso de querer acceder al paso dos sin pasar por el 1 redirecciono al 1
if (!$_POST) {
    header("Location: 1paso.php");
}
//leemos el numero de cajas a dibujar
$numeroCajas = $_POST["numero"];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paso 2</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="color1 rounded text-color1 p-5">
                <h1>Ejercicio Numero 1 del examen de PHP</h1>
                <div class="lead">Escriba algo en las cajas de texto</div>
            </div>
        </div>
        <form class="row mt-5 bg-light p-5" method="POST" action="3paso.php">
            <div class="mb-3">
                <h2 class="text-color1">
                    Paso 2
                </h2>
            </div>
            <?php
            for ($contador = 0; $contador < $numeroCajas; $contador++) {
                include "_cajas.php";
            }
            ?>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Paso 3</button>
                <button type="reset" class="btn btn-danger">Borrar</button>
            </div>
        </form>

        <div class="row mt-5 mb-0">
            <div class="text-color1 p-5 alert color1">
                Ejercicio del examen de PHP - Maheva Morales
            </div>
        </div>

        <div class="row mt-2">
            <img src="../images/foto4.jpg" class="col-lg-2 col-sm-2 d-block mx-auto">
        </div>
    </div>
</body>

</html>