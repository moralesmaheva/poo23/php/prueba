<?php 
use clases\ejercicio5\Articulo;
use clases\ejercicio5\ArticuloRebajado;

require_once 'autoload.php';

$articulo1= new Articulo("Laptop", 2000);
echo "<br>";
echo $articulo1->getNombre();
echo "<br>";
echo $articulo1->getPrecio();
echo "<br>";
echo $articulo1;

$articuloRebajado= new ArticuloRebajado("televisión", 1000, 10);
echo "<br>";
echo $articuloRebajado->getNombre();
echo "<br>";
echo $articuloRebajado->getPrecio();
echo "<br>";
echo $articuloRebajado;
echo "<br>";
echo $articuloRebajado->precioRebajado();

