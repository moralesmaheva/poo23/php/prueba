<?php

namespace Clases\Ejercicio3;

class Imagen
{
    const RUTA = 'images/';
    const PLANTILLA = __NAMESPACE__ . "/plantilla.blade.php";
    private ?string $src;
    private ?bool $border;
    private ?int $ancho;
    private ?int $alto;


    public function __construct(string $src, int $ancho = null, int $alto = null)
    {
        $this->src = $src;
        $this->border = $border = 0;
        $this->ancho = $ancho;
        $this->alto = $alto;
    }

    // //crear html para imprimir la imagen con la constante ruta y los atributos
    // public function __toString()
    // {
    //     $salida = "<img";
    //     $salida .= $this->getSrc() . $this->getBorder() . $this->getAncho() . $this->getAlto();
    //     $salida .= ">";
    //     return $salida;
    // }

    // --realizado con control de flujo--
    // public function __toString()
    // {
    //     ob_start();
    //     //asigno los valores a mostrar de la plantilla
    //     $src=$this->getSrc();
    //     $border=$this->getBorder();
    //     $ancho=$this->getAncho();
    //     $alto=$this->getAlto();
    //     //cargo la plantilla
    //     include 'plantilla.php';
    //     //devuelvo el contenido del buffer
    //     return ob_get_clean();
    // }

    //--voy a utilizar una plantilla--
    public function __toString()
    {
        $salida = file_get_contents(self::PLANTILLA);
        return str_replace(
            [
                "{{ src }}",
                "{{ border }}",
                "{{ ancho }}",
                "{{ alto }}"
            ],
            [
                $this->getSrc(),
                $this->getBorder(),
                $this->getAncho(),
                $this->getAlto()
            ],
            $salida
        );
    }



    /**
     * Get the value of scr
     *
     * @return ?string
     */
    public function getSrc(): ?string
    {
        $salida = " src='" . self::RUTA . $this->src . "' ";
        return $salida;
    }

    /**
     * Set the value of scr
     *
     * @param ?string $scr
     *
     * @return self
     */
    public function setSrc(?string $src): self
    {
        $this->src = $src;

        return $this;
    }

    /**
     * Get the value of border
     *
     * @return ?bool
     */
    public function getBorder(): string
    {
        return " border='" . (int)$this->border . "' ";
    }

    /**
     * Set the value of border
     *
     * @param ?bool $border
     *
     * @return self
     */
    public function setBorder(?bool $border): self
    {
        $this->border = $border;

        return $this;
    }

    /**
     * Get the value of ancho
     *
     * @return ?int
     */
    public function getAncho(): string
    {
        if (empty($this->ancho)) {
            return "";
        } else {
            return " width='" . $this->ancho . "' ";
        }
    }


    /**
     * Set the value of ancho
     *
     * @param ?int $ancho
     *
     * @return self
     */
    public function setAncho(?int $ancho): self
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get the value of alto
     *
     * @return ?int
     */
    public function getAlto(): string
    {
        if (empty($this->alto)) {
            return "";
        } else {
            return " height='" . $this->alto . "' ";
        }
    }


    /**
     * Set the value of alto
     *
     * @param ?int $alto
     *
     * @return self
     */
    public function setAlto(?int $alto): self
    {
        $this->alto = $alto;

        return $this;
    }
}
