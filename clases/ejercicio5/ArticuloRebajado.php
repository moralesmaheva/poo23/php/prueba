<?php

namespace clases\ejercicio5;

final class ArticuloRebajado extends Articulo
{
    private ?int $rebaja;


    public function __construct(?string $nombre = null, ?float $precio = 0, ?int $rebaja = 0)
    {
        parent::__construct($nombre, $precio);
        $this->rebaja = $rebaja;
    }

    private function calculaDescuento()
    {
        $descuento = $this->rebaja * $this->precio / 100;
        return $descuento;
    }

    public function precioRebajado()
    {
        $precioRebajado = $this->precio - $this->calculaDescuento();
        return $precioRebajado;
    }

    public function __toString()
    {
        return parent::__toString() .
            "La rebaja es: " . $this->rebaja . "% y el descuento: " . $this->calculaDescuento() . "€<br>";
    }
}
