<?php

namespace Clases\Ejercicio2;

class Imagen
{
    const RUTA = 'images/';
    const PLANTILLA = __NAMESPACE__ . "/plantilla.blade.php";
    private ?string $src;
    private ?bool $border;
    private ?int $ancho;
    private ?int $alto;


    public function __construct(?string $src, ?int $ancho = null, ?int $alto = null)
    {
        $this->src = $src;
        $this->border = $border = 0;
        $this->ancho = $ancho;
        $this->alto = $alto;
    }

    // --realizado directamente con un string--
    // public function __toString()
    // {
    //     return "<img src='" . self::RUTA . $this->src .
    //         "' width='" . $this->ancho . "' height='" . $this->alto .
    //         "' border='" . $this->border . "'>";
    // }


    // --realizado con control de flujo--
    // public function __toString()
    // {
    //     ob_start();
    //     //asigno los valores a mostrar de la plantilla
    //     $src=self::RUTA . $this->src;
    //     $border=$this->border;
    //     $ancho=$this->ancho;
    //     $alto=$this->alto;
    //     //cargo la plantilla
    //     include 'plantilla.php';
    //     //devuelvo el contenido del buffer
    //     return ob_get_clean();
    // }

    //--voy a utilizar una plantilla--
    public function __toString()
    {
        $salida = file_get_contents(self::PLANTILLA);
        return str_replace(
            [
                "{{ src }}",
                "{{ border }}",
                "{{ ancho }}",
                "{{ alto }}"
            ],
            [
                self::RUTA . "$this->src",
                $this->border ?: "0",
                $this->ancho,
                $this->alto
            ],
            $salida
        );

    }
}
