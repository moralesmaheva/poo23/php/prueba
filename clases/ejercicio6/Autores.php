<?php

namespace clases\ejercicio6;

use mysqli;

class Autores
{
    public ?int $id;
    public ?string $nombre;
    public ?string $apellidos;



    public function insertar($nombre, $apellidos)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $conexion = new mysqli("localhost", "root", "", "PruebaPhp");

        if ($conexion->connect_error) {
            die("Error en la conexión: " . $conexion->connect_error);
        }

        $sql = "INSERT INTO Autores (nombre,apellidos) VALUES (
            '"  . $this->nombre . "','" . $this->apellidos . "'
            )";

            if($conexion->query($sql)){
                return "Registro insertado";
            }else{
                return "Error al insertar" . $conexion->error;
            }
    }




    public function listar(){

        $conexion = new mysqli("localhost", "root", "", "PruebaPhp");

        if ($conexion->connect_error) {
            die("Error en la conexión: " . $conexion->connect_error);
        }

        $sql = "SELECT * FROM Autores";

        $resultados = $conexion->query($sql);
        $registros = $resultados->fetch_all(MYSQLI_ASSOC);

        $salida = "<h1>Autores</h1>";
        foreach ($registros as $registro) {
            $salida .= "<ul>";
            foreach ($registro as $campo => $valor) {
                $salida .= "<li>$valor</li>";
            }
            $salida .= "</ul>";
        }
            return $salida;

    }




    public function eliminar(int $id)
    {
        $conexion = new mysqli("localhost", "root", "", "PruebaPhp");

        if ($conexion->connect_error) {
            die("Error en la conexión: " . $conexion->connect_error);
        }
        $sql = "DELETE FROM Autores WHERE id = $id";
        
        if($conexion->query($sql)){
            return "Registro eliminado";
        }else{
            return "Error al eliminar" . $conexion->error;
        }
        
    }
}
