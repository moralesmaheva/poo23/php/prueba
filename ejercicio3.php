<?php 
use Clases\Ejercicio3\Imagen;

require_once 'autoload.php';

$imagen1 = new Imagen('foto1.jpg', 200, 200);

echo $imagen1;

$imagen2= new Imagen('foto2.jpg', 100, 100);

echo $imagen2;

$imagen3= new Imagen('foto3.png', 100, 100);

echo $imagen3;

$imagen4= new Imagen('foto4.jpg', 100, 100);

echo $imagen4;


$imagen1 -> setAlto(300);

echo $imagen1;

$imagen2 -> setBorder(true);

echo $imagen2;

$imagen3 -> setBorder(true);
$imagen3 -> setAncho(300);

echo $imagen3;